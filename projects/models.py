from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):  # Feature 3
    name = models.CharField(max_length=200)
    description = models.TextField()

    owner = models.ForeignKey(
        User,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):  # returns name of the project
        return self.name
